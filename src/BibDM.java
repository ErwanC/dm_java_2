import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static int plus(int a, int b){
      return a+b;
  }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer res=null;
      for (Integer i : liste)
        {
          if (res.equals(null) || i<res){res=liste.get(i);}
        }
       return res;
  }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    for (T nb : liste)
    {
      if(nb.compareTo(valeur)<0){return false;}
    }
      return true;
  }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> res=new ArrayList<>();
      for (T nb : liste1)
      {
        if (liste2.contains(nb)){if(false==res.contains(nb)){res.add(nb);}}
      }
      return res;
  }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
    List<String> res=new ArrayList<String>();
    String mot="";
    for (int i=0; i<texte.length(); i++ )
    {
      if (texte.charAt(i)==' ')
      {
        if(mot.compareTo("")!=0){res.add(mot);}
      }
      else {mot=mot+texte.charAt(i);}
    }
    if(mot.compareTo("")!=0){res.add(mot);}
    return res;
  }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      List<String> listeDeMot=decoupe(texte);
      Map<String,Integer> map= new HashMap<String,Integer>();
      for (String mot : listeDeMot)
      {
        Integer nombre=map.get(mot);
        if (nombre==null)
        {
          nombre=0;
        }
        nombre++;
        map.put(mot,nombre);
      }
      String res=null;
      for (String mot : map.keySet())
      {
        if (res.equals(null) || map.get(mot)>map.get(res))
        {
          res=mot;
        }
        else if (map.get(mot).equals(map.get(res)))
        {
          if (map.get(mot).compareTo(map.get(res))<0)
          {
            res=mot;
          }
        }
      }
      return res;
  
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
   int tailleMot=chaine.length();
   int nombreParen=0;
   if (tailleMot%2!=0){return false;}
   else
   {
     for (int i=0; i<tailleMot; i++ )
     {
       if (chaine.charAt(i)=='('){nombreParen+=1;}
       if (chaine.charAt(i)==')')
	{
	  if (nombreParen==0){return false;}
          else{nombreParen-=1;}
	}
     }
   }
   if(nombreParen!=0){return false;}
   return true;
 }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
    int tailleMot=chaine.length();
    int nombreParen=0;
    int nombreCrochet=0;
    if (tailleMot%2!=0){return false;}
    else
    {
      for (int i=0; i<tailleMot; i++ )
      {
       if (chaine.charAt(i)=='('){nombreParen+=1;}
       if (chaine.charAt(i)=='['){nombreCrochet+=1;}
       if (chaine.charAt(i)==')')
	{
	  if (nombreParen==0){return false;}
          if (nombreCrochet!=0){return false;}
          else{nombreParen-=1;}
	}
       if (chaine.charAt(i)==']')
	{
	  if (nombreCrochet==0){return false;}
          if (nombreParen!=0){return false;}
          else{nombreCrochet-=1;}
	}
      }
    }
    return true;
  }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      Integer a=0;
      Integer b=liste.size()-1;
      Integer milieu=(a+b)/2;
      while (a<b)
      {
        if (liste.get(milieu).equals(valeur))
        {
          return true;
        }
        else if (liste.get(milieu).compareTo(valeur)<0)
        {
          b=milieu-1;
        }
        else
        {
          a=milieu+1;
        }
        milieu=(a+b)/2;
      }
      return false;
  }


}
